/*
 * =====================================================================================
 *
 *       Filename:  BTB.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  04/05/2020 08:40:44 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Elliott Villars (), elliottvillars@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef enum STATE{
	STRONG_TAKEN,
	WEAK_TAKEN,
	WEAK_NOT_TAKEN,
	STRONG_NOT_TAKEN
}STATE;

typedef struct BTB
{
	unsigned int entry;
	char currentPC[16];
	char targetPC[16];
	STATE prediction;
	unsigned int used;
}BTB;

//STATE class_state_Machine = STRONG_TAKEN;

STATE changeClassPredictionTaken(STATE entrySTATE);
STATE changeClassPredictionNotTaken(STATE entrySTATE);

STATE changeLocalPredictionTaken(STATE entryState);
STATE changeLocalPredictionNotTaken(STATE entryState);
