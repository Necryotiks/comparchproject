#include "BTB.h"
// added class state machine
//Class State Machine
STATE changeClassPredictionTaken(STATE entrySTATE) {
	STATE new_state;
	switch(entrySTATE) {
		case STRONG_TAKEN:
			new_state = STRONG_TAKEN;
			break;
		case WEAK_TAKEN:
			new_state = STRONG_TAKEN;
			break;
		case WEAK_NOT_TAKEN:
			new_state = WEAK_TAKEN;
			break;
		case STRONG_NOT_TAKEN:
			new_state = WEAK_NOT_TAKEN;
			break;
	}
	return new_state;
}

STATE changeClassPredictionNotTaken(STATE entrySTATE) {
	STATE new_state;
	switch(entrySTATE) {
		case STRONG_TAKEN:
			new_state = WEAK_TAKEN;
			break;
		case WEAK_TAKEN:
			new_state = WEAK_NOT_TAKEN;
			break;
		case WEAK_NOT_TAKEN:
			new_state = STRONG_NOT_TAKEN;
			break;
		case STRONG_NOT_TAKEN:
			new_state = STRONG_NOT_TAKEN;
			break;
	}
	return new_state;
}

//STATE MACHINE B
STATE changeLocalPredictionTaken(STATE entryState)
{
	STATE new_state;
	switch(entryState)
	{
		case STRONG_TAKEN:
			new_state = STRONG_TAKEN;
			break;
		case WEAK_TAKEN:
			new_state = STRONG_TAKEN;
			break;
		case WEAK_NOT_TAKEN:
			new_state = STRONG_TAKEN;
			break;
		case STRONG_NOT_TAKEN:
			new_state = WEAK_NOT_TAKEN;
			break;
	}
	return new_state;
}

STATE changeLocalPredictionNotTaken(STATE entryState)
{
	STATE new_state;
	switch(entryState)
	{
		case STRONG_TAKEN:
			new_state = WEAK_TAKEN;
			break;
		case WEAK_TAKEN:
			new_state = WEAK_NOT_TAKEN;
			break;
		case WEAK_NOT_TAKEN:
			new_state = STRONG_NOT_TAKEN;
			break;
		case STRONG_NOT_TAKEN:
			new_state = STRONG_NOT_TAKEN;
			break;
	}
	return new_state;
}

