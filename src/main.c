/*
 * =====================================================================================
 *
 *       Filename:  main.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  04/05/2020 08:07:36 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Elliott Villars (), elliottvillars@gmail.com
 *   Organization:  
 *
 * =====================================================================================
 */

#include "BTB.h"
#include <libgen.h>

unsigned int instruction_count = 0;
unsigned int number_of_hits = 0;
unsigned int number_of_misses = 0;
unsigned int number_of_right_predictions = 0;
unsigned int number_of_wrong_predictions = 0;
unsigned int number_of_taken_branches = 0;
unsigned int number_of_collisions = 0;
unsigned int number_of_wrong_address_predicitons = 0;
unsigned int hit_rate = 0;
unsigned int incorrect_address_percentage = 0;
FILE * fh;
// added outfile
FILE *outfile;
BTB btb_array[1024] = {0};
char fileBuffer[10000000][16];

//TODO: update prediction state machine.
int main(int argc, char ** argv)
{
	if(argc == 1 || argc > 2)
	{
		printf("Invalid argument count.");
		exit(-4);
	}
	char * fstr = argv[1];
	fh = fopen(fstr,"r");
	char * bname = basename(fstr); //could segfault
	char buf[64];
	strcpy(buf,"./");
	strcat(buf,bname);
	strcat(buf,".out");

	// outfile to see result of btb and variables
	outfile = fopen(buf,"w");

	char currentLine[16];
	//Load file into buffer
	while(fgets(currentLine,sizeof(currentLine),fh) != NULL)
	{
		int len;
		strcpy(fileBuffer[instruction_count],currentLine);
		len = strlen(fileBuffer[instruction_count]);
		// changed to len-2 to get rid of trailing newline
		fileBuffer[instruction_count][len-2] = '\0';
		instruction_count++;
	}
	//Identify branches here
	for(unsigned int i = 0; i < instruction_count; i++)
	{
		unsigned int currentPC;
		unsigned int nextPC;
		unsigned int idx;
		currentPC = (unsigned int)strtol(fileBuffer[i],NULL,16);
		nextPC = (unsigned int)strtol(fileBuffer[i+1],NULL,16);
		idx = (currentPC & 0xFFF) >> 2;
		STATE current_class_state;
		STATE current_local_state;
		if(strcmp(btb_array[idx].currentPC,fileBuffer[i]) != 0)//if entry is not found
		{
			if(nextPC == currentPC + 4)
			{
				continue;
			}
			else
			{
				number_of_misses++;
				number_of_taken_branches++;
				if(btb_array[idx].used == 1)
				{
					number_of_collisions++;
				} 
				btb_array[idx].entry = idx;
				strcpy(btb_array[idx].currentPC,fileBuffer[i]);
				strcpy(btb_array[idx].targetPC,fileBuffer[i+1]);
				btb_array[idx].prediction = STRONG_TAKEN; //Check this
				btb_array[idx].used = 1;
			}
		}
		else
		{
			number_of_hits++;
			//check predictions here
			if(btb_array[idx].prediction == STRONG_TAKEN || btb_array[idx].prediction == WEAK_TAKEN)
			{
				if(nextPC == strtol(btb_array[idx].targetPC,NULL,16))
				{
					number_of_right_predictions++;
					number_of_taken_branches++;
					// added state changes
					current_local_state = changeLocalPredictionTaken(btb_array[idx].prediction);
					btb_array[idx].prediction = current_local_state;
				}
				else
				{
					number_of_wrong_predictions++;
					if(nextPC != currentPC + 4)
					{
						number_of_wrong_address_predicitons++;
						number_of_taken_branches++;
						// added wrong address update
						strcpy(btb_array[idx].targetPC,fileBuffer[i+1]);
						// added state changes
						current_local_state = changeLocalPredictionTaken(btb_array[idx].prediction);
					}
					else
					{
						//TODO: change to not take
						current_local_state = changeLocalPredictionTaken(btb_array[idx].prediction);
					}
					btb_array[idx].prediction = current_local_state;
				}
			}
			else
			{
				if(nextPC == currentPC + 4)
				{
					number_of_right_predictions++;
					//NOT TAKEN
					current_local_state = changeLocalPredictionTaken(btb_array[idx].prediction);
					btb_array[idx].prediction = current_local_state;
				}
				else
				{
					number_of_wrong_predictions++;
					number_of_taken_branches++;
					// added state changes
					current_local_state = changeLocalPredictionTaken(btb_array[idx].prediction);
					btb_array[idx].prediction = current_local_state;

				}
			}
		}
	}
	// print out btb array to file
	for(int i = 0; i < 1024; i++) {
		if(btb_array[i].entry != 0)
		{
			char * predstr;
			switch(btb_array[i].prediction)
			{
				case STRONG_TAKEN:
					predstr = "00";
					break;
				case WEAK_TAKEN: 
					predstr = "01";
					break;
				case WEAK_NOT_TAKEN:
					predstr = "10";
					break;
				case STRONG_NOT_TAKEN:
					predstr = "11";
					break;
			}
			fprintf(outfile, "%d %s %s %s\n", btb_array[i].entry, btb_array[i].currentPC, btb_array[i].targetPC, predstr);
		}

	}
	// print out final values for every variable
	fprintf(outfile, "Hits: %d\nMisses: %d\nRight: %d\nWrong: %d\nTaken: %d\nCollisions: %d\nWrong Address: %d\n",
			number_of_hits, number_of_misses, number_of_right_predictions, number_of_wrong_predictions, 
			number_of_taken_branches, number_of_collisions, number_of_wrong_address_predicitons);
	// print out calculated values
	fprintf(outfile, "Hit Rate: %f%%\nPrediction Accuracy: %f%%\nIncorrect Address:%f%%\n", 
			100*(double)((double)number_of_hits/((double)number_of_hits+(double)number_of_misses)),
			100*(double)((double)number_of_right_predictions/(double)number_of_hits), 
			100*(double)number_of_wrong_address_predicitons/(double)number_of_misses);
	fclose(outfile);
	fclose(fh);
}
